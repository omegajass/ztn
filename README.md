# Zero Trust Networks

In the scope of my master thesis at the technical university of cluj-napoca I created this repository to make it easily shareable between peers.

## Getting Started

The experiment was to test the robustness of the Zero-Trust Network architecture against common attacks.
First we set up the infrastructure coded in python and make the build and deploy automated with docker.

### Prerequisites

To run this setup please install docker.io and docker-compose

```
apt-install docker.io
apt install python3-pip
pip3 install docker-compose

Mobile app: Google Authenticator
```

## Mapping

The project was developed in python as local infrastructre as a code.
Please consult this table to access the services.

| Service          |Port       |   
|:----------------:|:---------:|
|Access Proxy      |8080       |
|Trust Engine      |8081       |
|Datastores        |8082       |
|HOTP              |8100       |
|OCSP              |8101       |
|HOME              |8102       |
|User enrollment   |8200       |
|HOTP enrollment   |8201       |


## Installing

Simply run the setup.sh script which will build and deploy the infrastructure on your localhost in form of dockers.

```
./setup.sh
```

**ZTN uses mutual authentication to authenticate users so take care to import the client certificate in your browser and system before accessing the systems.**

Simply access localhost to start the user enrollement process, download your certificate, get your QR Code which is compatible with google authenticator.

## Enroll user

Navigate to `cd /opt/ztn/apps/enroll_user/` folder and run `python3 enroll_user.py`

The service will be accessible on port 8200.
Make sur to take the pin code and insert it in the respectiv field.

Certificate download will start automatically.
Go to Settings -> Certificates and import the downloaded certificate.

Connect to the home endpoint to verify connetivity.

**Make sure to shutdown the service after the completion.**

### Backup
As soon as the endpoint is hit, the client certificate should be destroyed.
The script thesis-encrypt_ssss.sh will generate a rando; 256bit key and encrypt the file.
The key will be split with Shamir Secret Sharing algorithm and the encrypted certificate will be placed in `/opt/vault`.
Make sure to distribute the keys to the concerned parties to reconstruct the decryption key.


## HOTP

Navigate to `cd /opt/ztn/apps/hotp/` folder and run `python3 hotp_enrollement.py`

The service will be accessible on port 8201.

**Make sure to have downloaded Google Authenticator from the playstore.**

Scan the QR Code and you are good to go.

## License

This project is licensed under the GNUv3 License - see the [LICENSE.md](LICENSE.md) file for details
