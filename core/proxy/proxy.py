#!/usr/bin/python3

from flask import Flask, request, Response, render_template, send_file
import werkzeug.serving
import ssl
import OpenSSL
import requests
import ssl
import json
import base64
from PIL import Image
import io
from base64 import b64encode

class PeerCertWSGIRequestHandler( werkzeug.serving.WSGIRequestHandler ):
    """
    We subclass this class so that we can gain access to the connection
    property. self.connection is the underlying client socket. When a TLS
    connection is established, the underlying socket is an instance of
    SSLSocket, which in turn exposes the getpeercert() method.

    The output from that method is what we want to make available elsewhere
    in the application.
    """
    def make_environ(self):
        """
        The superclass method develops the environ hash that eventually
        forms part of the Flask request object.

        We allow the superclass method to run first, then we insert the
        peer certificate into the hash. That exposes it to us later in
        the request variable that Flask provides
        """
        environ = super(PeerCertWSGIRequestHandler, self).make_environ()
        x509_binary = self.connection.getpeercert(True)
        x509 = OpenSSL.crypto.load_certificate( OpenSSL.crypto.FILETYPE_ASN1, x509_binary )
        environ['peercert'] = x509
        return environ

app = Flask(__name__)

def validate_request(hash):
    data = {}
    data['hash'] = hash
    json_data = json.dumps(data)
    r = requests.post("https://localhost:8101/check", verify=False, data=json_data, cert=('/opt/certs/servers/accessproxy/accessproxy.crt', '/opt/certs/servers/accessproxy/accessproxy.key'))
    if bool(int(r.content)):
        return False
    else:
        return True

def forward_request_home(hash, url, subject):
    data = {}
    data['hash'] = hash
    data['url'] = url
    data['CN'] = subject.CN
    data['OU'] = subject.OU
    json_data = json.dumps(data)
    r = requests.post("https://localhost:8102", verify=False, data=json_data, cert=('/opt/certs/servers/accessproxy/access_proxy.crt', '/opt/certs/servers/accessproxy/access_proxy.key'))
    return r

def forward_request(hash, url, subject):
    data = {}
    data['hash'] = hash
    data['url'] = url
    data['CN'] = subject.CN
    data['OU'] = subject.OU
    json_data = json.dumps(data)
    r = requests.post("https://localhost:8081", verify=False, data=json_data, cert=('/opt/certs/servers/accessproxy/accessproxy.crt', '/opt/certs/servers/accessproxy/accessproxy.key'))
    return r

def forward_hotp_verify(hash, url, subject):
    data = {}
    data['hash'] = hash
    data['url'] = url
    data['CN'] = subject.CN
    data['OU'] = subject.OU
    json_data = json.dumps(data)
    r = requests.post("https://localhost:8102/verify", verify=False, data=json_data, cert=('/opt/certs/servers/accessproxy/accessproxy.crt', '/opt/certs/servers/accessproxy/accessproxy.key'))
    return r

def forward_hotp_request(hash, url, subject):
    data = {}
    data['hash'] = hash
    data['url'] = url
    data['CN'] = subject.CN
    data['OU'] = subject.OU
    json_data = json.dumps(data)
    r = requests.post("https://localhost:8102/request", verify=False, data=json_data, cert=('/opt/certs/servers/accessproxy/accessproxy.crt', '/opt/certs/servers/accessproxy/accessproxy.key'))
    return r

@app.route('/favicon.ico')
def response():
    return Response(status=200)

@app.route('/verify', methods=['POST'])
def verify():
    hash = request.environ['peercert'].digest("sha256").decode("utf-8")
    hash = hash.replace(":","")
    url = request.url
    subject = request.environ['peercert'].get_subject()
    hotp = int(request.form['hotp'])
    hotp_counter = forward_hotp_verify(hash, url, subject)

    print(hotp)
    hotp_counter_json = json.loads(hotp_counter.content)

    if hotp == hotp_counter_json:
        return Response("Accepted")
    else:
        return Response("Rejected")

@app.route('/', defaults={'u_path': ''})
@app.route('/<path:u_path>')
def catch_all(u_path):
    hash = request.environ['peercert'].digest("sha256").decode("utf-8")
    hash = hash.replace(":","")
    url = request.url
    subject = request.environ['peercert'].get_subject()
    direction = url.split("/")[-1]

    if validate_request(hash) == "0":
        trust_engine_response = forward_request(hash, url, subject)
        print(trust_engine_response)
        trust_engine_response_data = json.loads(trust_engine_response.content)
        return Response("Works", mimetype="text/html")

    else:
        #Certificate trying to access the server is revoked
        return Response(status=401)

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/accessproxy/accessproxy.crt', '/opt/certs/servers/accessproxy/accessproxy.key')
    app.run('0.0.0.0', 8080, ssl_context=context, request_handler=PeerCertWSGIRequestHandler)
