#!/usr/bin/python3

from flask import Flask, request, Response
import werkzeug.serving
import ssl
import OpenSSL
import ssl
import json
import requests

class PeerCertWSGIRequestHandler( werkzeug.serving.WSGIRequestHandler ):
    """
    We subclass this class so that we can gain access to the connection
    property. self.connection is the underlying client socket. When a TLS
    connection is established, the underlying socket is an instance of
    SSLSocket, which in turn exposes the getpeercert() method.

    The output from that method is what we want to make available elsewhere
    in the application.
    """
    def make_environ(self):
        """
        The superclass method develops the environ hash that eventually
        forms part of the Flask request object.

        We allow the superclass method to run first, then we insert the
        peer certificate into the hash. That exposes it to us later in
        the request variable that Flask provides
        """
        environ = super(PeerCertWSGIRequestHandler, self).make_environ()
        x509_binary = self.connection.getpeercert(True)
        x509 = OpenSSL.crypto.load_certificate( OpenSSL.crypto.FILETYPE_ASN1, x509_binary )
        environ['peercert'] = x509
        return environ

app = Flask(__name__)
def forward_request_device(hash):
    requests.post("https://localhost:8090/device", verify=False, data=hash, cert=('/opt/certs/servers/trustengine/trustengine.crt', '/opt/certs/servers/trustengine/trustengine.key'))

def forward_request_user(hash):
    requests.post("https://localhost:8090/user", verify=False, data=hash, cert=('/opt/certs/servers/trustengine/trustengine.crt', '/opt/certs/servers/trustengine/trustengine.key'))

def forward_request_netflows(hash):
    requests.post("https://localhost:8090/netflows", verify=False, data=hash, cert=('/opt/certs/servers/trustengine/trustengine.crt', '/opt/certs/servers/trustengine/trustengine.key'))

def forward_request_security(hash):
    requests.post("https://localhost:8090/security", verify=False, data=hash, cert=('/opt/certs/servers/trustengine/trustengine.crt', '/opt/certs/servers/trustengine/trustengine.key'))

@app.route('/', defaults={'u_path': ''}, methods=["GET","POST"])
@app.route('/<path:u_path>', methods=["GET","POST"])
def catch_all(u_path):
    data = request.data

    response = {}

    device_result = forward_request_device(data)
    user_result = forward_request_user(data)
    netflows_result = forward_request_netflows(data)
    security_result = forward_request_security(data)

    response['status'] = "Allow"
    data = json.dumps(response)
    return Response(response=data, mimetype="application/json")

    response = {}
    response['status'] = "Allow"
    data = json.dumps(response)
    print(data)
    return Response(response=data, mimetype="application/json")


if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/trustengine/trustengine.crt', '/opt/certs/servers/trustengine/trustengine.key')
    app.run('0.0.0.0', 8081, ssl_context=context, request_handler=PeerCertWSGIRequestHandler)
