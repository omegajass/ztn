#!/usr/bin/python3
from tinydb import TinyDB, Query
import tinydb_encrypted_jsonstorage as tae

KEY = "users"
PATH = "users/user.json"
db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

#User 1 certificate Hash
db.insert({"hash":"C00901F4B1F778AE7E64D30A9EAAA85B55FA1D95361B43B483223AE833B0B5CA"})

KEY = "devices"
PATH = "devices/device.json"
db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)
