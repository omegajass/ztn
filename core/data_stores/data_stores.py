#!/usr/bin/python3

from flask import Flask, request, Response
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
import tinydb_encrypted_jsonstorage as tae
import ssl
import json

app = Flask(__name__)


KEY = "users"
PATH = "users/user.json"
users_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

KEY = "devices"
PATH = "devices/device.json"
devices_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

KEY = "netflows"
PATH = "devices/netflows.json"
devices_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

KEY = "security"
PATH = "devices/security.json"
devices_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

def check_user(hash):
    User = Query()
    response = users_db.count(User['hash'] == hash)
    return response

@app.route('/device',methods=["POST"])
def device():
    return Response("Working", mimetype="text/html")

@app.route('/user',methods=["POST"])
def user():
    return Response("Working", mimetype="text/html")

@app.route('/netflows',methods=["POST"])
def netflows():
    return Response("Working", mimetype="text/html")

@app.route('/security',methods=["POST"])
def security():
    return Response("Working", mimetype="text/html")


if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/datastore/datastore.crt', '/opt/certs/servers/datastore/datastore.key')
    app.run('0.0.0.0', 8090, ssl_context=context)
