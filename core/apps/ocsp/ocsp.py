from flask import Flask, request
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
import tinydb_encrypted_jsonstorage as tae
import ssl
import json

app = Flask(__name__)

KEY = "crl"
PATH = "./crl.json"
crl_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

# Checks the database for the presence of the hash of the requesting user's certificate
def check_fingerprint(hash):
    fingerprint = Query()
    response = crl_db.count(fingerprint['hash'] == hash)
    return response

# API endpoint to revoke a given client certificate
@app.route('/revoke', methods=["POST"])
def revoke(hash):
    data = json.loads(request.data)
    crl_db.insert({"hash":data['hash']})
    return 'Removed'

# Returns to the accessproxy 1 if certificate revoked and 0 if not
@app.route('/check', methods=["POST"])
def catch_all():
    data = json.loads(request.data)
    if bool(check_fingerprint(data['hash'])):
        return str(1)
    else:
        return str(0)

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/ocsp/ocsp.crt', '/opt/certs/servers/ocsp/ocsp.key')
    app.run('0.0.0.0', 8101, ssl_context=context)
