#!/usr/bin/python3
from tinydb import TinyDB, Query
import tinydb_encrypted_jsonstorage as tae
import sys

KEY = "crl"
PATH = "./crl.json"
crl_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

hash = sys.argv[1].replace(":","")

#User 1 certificate Hash
crl_db.insert({"hash":hash})
