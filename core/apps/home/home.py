from flask import Flask, request, Response, send_file
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
import ssl
import json

app = Flask(__name__)

# Home for a successful installed certificate
@app.route('/home', methods=["GET","POST"])
def verify():
    content = """ <h1> Hello </h1>
            """

    return Response(content, mimetype="text/html")

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/home/home.crt', '/opt/certs/servers/home/home.key')
    app.run('0.0.0.0', 8102, ssl_context=context)
