from flask import Flask, request, Response, send_file
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
import ssl
import json
import os
import random
import string

# Generates the pin asked when accessing the certificate download page
def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

app = Flask(__name__)
db = TinyDB('db.json')

# On the spot client.p12 certificate generation
os.system("/opt/bins/thesis-generate_user.sh client")
keyword = randomString(8)
print(keyword)

# Security check to only serve a certificate once
def check_user(hash):
    User = Query()
    response = db.count(User['hash'] == hash)
    return response

# Get certificate endpoint
@app.route('/get_cert')
def catch_all():
    content = """ <form action="/testpathpin" method="post">
                  <label for="secret">Pin:</label>
                  <input type="text" id="secret" name="secret"><br><br>
                  <input type="submit" value="Submit">
                </form>
            """
    return Response(content, mimetype="text/html")

# Endpoint responding with the certificate if correct pin is inserted
@app.route('/testpathpin', methods=["POST"])
def verify():
    content = request.form['secret']
    if content == keyword:
        return send_file("client.p12",'client.p12', as_attachment=True)
    else:
        return "Error"

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain('/opt/certs/servers/euser/euser.crt', '/opt/certs/servers/euser/euser.key')
    app.run('0.0.0.0', 8200, ssl_context=context)
