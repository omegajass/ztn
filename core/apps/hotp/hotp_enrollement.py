#!/usr/bin/python3

from flask import Flask, request, Response, send_file
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
from tinydb.operations import increment
import tinydb_encrypted_jsonstorage as tae
import ssl
import json
import sys
import pyotp
import qrcode
import requests
from PIL import Image
from io import BytesIO

app = Flask(__name__)

KEY = "hotp"
PATH = "./hotp.json"
hotp_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

class PeerCertWSGIRequestHandler( werkzeug.serving.WSGIRequestHandler ):
    """
    We subclass this class so that we can gain access to the connection
    property. self.connection is the underlying client socket. When a TLS
    connection is established, the underlying socket is an instance of
    SSLSocket, which in turn exposes the getpeercert() method.

    The output from that method is what we want to make available elsewhere
    in the application.
    """
    def make_environ(self):
        """
        The superclass method develops the environ hash that eventually
        forms part of the Flask request object.

        We allow the superclass method to run first, then we insert the
        peer certificate into the hash. That exposes it to us later in
        the request variable that Flask provides
        """
        environ = super(PeerCertWSGIRequestHandler, self).make_environ()
        x509_binary = self.connection.getpeercert(True)
        x509 = OpenSSL.crypto.load_certificate( OpenSSL.crypto.FILETYPE_ASN1, x509_binary )
        environ['peercert'] = x509
        return environ

# Security check to serve a QR Code once to a given certificate
def check_hash(hash):
    HOTP = Query()
    response = hotp_db.count(HOTP['hash'] == hash)
    return response

# QR Code for google authenticator provider
@app.route('/request', methods=['GET'])
def request_code():
    hash = request.environ['peercert'].digest("sha256").decode("utf-8")
    subject = request.environ['peercert'].get_subject()

    if not bool(check_hash(hash)):

        user = str(subject.CN)
        email = user+"@thesis.local"

        hotp = pyotp.random_base32()
        hotpp = pyotp.HOTP(hotp)

        hotp_db.insert({"hash":hash, "secret":hotp, "counter":1})
        link = pyotp.hotp.HOTP(hotp).provisioning_uri(email, initial_count=0, issuer_name="Thesis2020")
        img = qrcode.make(link)
        byte_io = BytesIO()
        img.save(byte_io, 'png')
        byte_io.seek(0)

        return send_file(byte_io, mimetype='image/png')
    else:
        return Response(status=401)

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/hotp/hotp.crt', '/opt/certs/servers/hotp/hotp.key')
    app.run('0.0.0.0', 8201, ssl_context=context, request_handler=PeerCertWSGIRequestHandler)
