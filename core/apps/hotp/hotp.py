#!/usr/bin/python3

from flask import Flask, request, Response, send_file
import werkzeug.serving
import ssl
import OpenSSL
from tinydb import TinyDB, Query
from tinydb.operations import increment
import tinydb_encrypted_jsonstorage as tae
import ssl
import json
import sys
import pyotp
import qrcode
import requests
from PIL import Image
from io import BytesIO

app = Flask(__name__)

KEY = "hotp"
PATH = "./hotp.json"
hotp_db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)

# Security to check if certificate is enrolled
def check_hash(hash):
    HOTP = Query()
    response = hotp_db.count(HOTP['hash'] == hash)
    return response

# API endpoint that verifies a given pin code against the database and increment the counter for next pin
@app.route('/verify', methods=['GET','POST'])
def verify():
    data = json.loads(request.data)
    hash = data['hash']
    if bool(check_hash(hash)):
        User = Query()
        result = hotp_db.search(User.hash == hash)
        print(result)
        counter = result[0]['counter']
        secret = result[0]['secret']

        hotp_db.update(increment("counter"), User.hash == hash)
        hotpp = pyotp.HOTP(secret)

        return Response(hotpp.at(counter))
    else:
        return Response(status=401)

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations('/opt/CA/ca-crt.pem')
    context.load_cert_chain('/opt/certs/servers/hotp/hotp.crt', '/opt/certs/servers/hotp/hotp.key')
    app.run('0.0.0.0', 8100, ssl_context=context)
