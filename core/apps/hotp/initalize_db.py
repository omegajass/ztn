#!/usr/bin/python3
from tinydb import TinyDB, Query
import tinydb_encrypted_jsonstorage as tae

KEY = "hotp"
PATH = "hotp.json"
db = TinyDB(encryption_key=KEY, path=PATH, storage=tae.EncryptedJSONStorage)
