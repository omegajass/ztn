#!/bin/sh

rm -r root-cert
rm -r certs

mkdir root-cert
cd root-cert
./../bins/thesis-generate_ca.sh
openssl x509 -outform der -in ca-crt.pem -out ca-crt.crt
cd ../
mkdir certs
mkdir -p certs/servers/accessproxy
mkdir -p certs/servers/datastore
mkdir -p certs/servers/hotp
mkdir -p certs/servers/ocsp
mkdir -p certs/servers/home
mkdir -p certs/servers/trustengine
mkdir -p certs/servers/euser

cd certs/servers/accessproxy
../../../bins/thesis-generate_and_sign.sh accessproxy
cd ../../../
cd certs/servers/euser
../../../bins/thesis-generate_and_sign.sh euser
cd ../../../
cd certs/servers/home
../../../bins/thesis-generate_and_sign.sh home
cd ../../../
cd certs/servers/datastore
../../../bins/thesis-generate_and_sign.sh datastore
cd ../../../
cd certs/servers/hotp
../../../bins/thesis-generate_and_sign.sh hotp
cd ../../../
cd certs/servers/ocsp
../../../bins/thesis-generate_and_sign.sh ocsp
cd ../../../
cd certs/servers/trustengine
../../../bins/thesis-generate_and_sign.sh trustengine
cd ../../../


docker build -t thesis2020 .
docker-compose up
