name=$1

# create $name private key and $name CSR
openssl req -nodes -new -keyout $name.key -out $name.csr

# generate certicate based on $name's CSR using CA root certificate and CA private key
openssl x509 -req -days 365 -in $name.csr -CA ../../../root-cert/ca-crt.pem -CAkey  ../../../root-cert/ca.key -CAcreateserial -out $name.crt

# verify the certificate (optionally)
openssl verify -CAfile  ../../../root-ca/ca-crt.pem $name.crt

# generate a browser importable certificate
openssl pkcs12 -export -inkey $name.key -in $name.crt -out $name.p12

#rm $name.crt $name.csr $name.key
