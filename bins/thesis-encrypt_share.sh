#!/bin/bash

openssl rand -base64 64 |  tr -d '\n'> key.bin
openssl aes-256-cbc -in $1 -out $1.enc -pass file:key.bin
mv client.p12 /opt/vault
ssss-split -t 3 -n 6 < key.bin
rm key.bin
