FROM ubuntu:18.04

RUN mkdir -p /opt/ztn/proxy
RUN mkdir -p /opt/bins
RUN mkdir -p /opt/certs
RUN mkdir -p /opt/CA
RUN mkdir -p /opt/vault

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.8 \
    python3-pip \
    openssl \
    supervisor && \
    apt-get autoremove --purge -y && \
    apt-get clean && \
    pip3 install flask \
    pyopenssl \
    requests \
    pillow \
    tinydb \
    tinydb_encrypted_jsonstorage \
    pyotp \
    qrcode

COPY ./bins /opt/bins
COPY ./root-cert /opt/CA
COPY ./root-cert/ca-crt.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates

COPY ./certs /opt/certs
COPY ./core /opt/ztn/

COPY supervisor/*.conf /etc/supervisor/conf.d/
COPY ./entrypoint.sh /

CMD ["./entrypoint.sh"]
